package com.apps;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Sample {
  public static void main(String[] args) {
    System.out.println("initial line of the main");
    System.out.println(Arrays.stream(fibonacci(10))
            .mapToObj((i) -> String.valueOf(i))
            .collect(Collectors.joining(", ", "[", "]")));
  }

  public static int[] fibonacci(int n){
    switch (n){
      case 0:
        throw new IllegalArgumentException();
      case 1:
        return new int[]{1};
      case 2:
        return new int[]{1,1};
    }
    int[] arr = new int[n];
    arr[0] = 1;
    arr[1] = 1;
    for(int i = 2 ; i < n; i ++){
      arr[i] = arr[i - 1] + arr[i - 2];
    }
    return arr;
  }
}
